package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

func main() {
	// Define and parse file path and debug flags.
	filePath := flag.String("file", "", "path to the JSON file")
	debugFlag := flag.Bool("debug", false, "print debug logs")
	flag.Parse()

	// Set the logging level to debug if debug flag is passed.
	if *debugFlag {
		log.SetLevel(log.DebugLevel)
	}

	log.Debugf("parsed JSON file path: %s", *filePath)

	// Load JSON map from the file path provided.
	loadedJSON, err := loadJSON(*filePath)
	if err != nil {
		log.Errorf("error loading file '%s': %s", *filePath, err)
		os.Exit(1)
	}
	log.Debugf("loaded JSON representation: %s", loadedJSON)

	// Call flattenJSON with the loaded JSON's map representation and empty result.
	result := map[string]interface{}{}
	flattenJSON("", loadedJSON.(map[string]interface{}), result)
	log.WithField("result", result).Info("result map")

	// Marshal the result map into JSON format.
	res, err := json.MarshalIndent(result, "", "    ")
	if err != nil {
		log.Errorf("error marshaling JSON: %s", err)
		os.Exit(1)
	}

	// Finally, output the marshalled JSON []byte into string format.
	log.Printf("final result:\n%s", res)
}

// loadJSON takes the path to the JSON file, reads the file
// and loads the JSON object into memory in a map format.
func loadJSON(path string) (interface{}, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error reading the file: %s", err)
	}

	var loadedJSON interface{}
	err = json.Unmarshal(data, &loadedJSON)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling json: %s", err)
	}

	return loadedJSON, nil
}

// flattenJSON takes hierarchical JSON map and converts into a flat JSON map representation.
func flattenJSON(pathSoFar string, loaded, result map[string]interface{}) {
	for k, val := range loaded {
		switch v := val.(type) {
		case map[string]interface{}:
			// If the value field is another map of string to interface{} then it means
			// it's another nested JSON object, and we need to process it further.
			if pathSoFar != "" {
				pathSoFar += fmt.Sprintf(".%s", k)
			} else {
				pathSoFar = k
			}

			// Recursively call flattenJSON for the sub-struct.
			flattenJSON(pathSoFar, v, result)
		default:
			// For any other types, it's a leaf object, so we can store the leaf value as-is.
			if pathSoFar != "" {
				log.Debugf("%s.%s: %v\n", pathSoFar, k, v)
				key := fmt.Sprintf("%s.%s", pathSoFar, k)
				result[key] = v
			} else {
				log.Debugf("%s: %v\n", k, v)
				result[k] = v
			}
		}
	}
}
