package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
)

var jsonTable = []struct {
	description string
	input       []byte
	output      []byte
}{
	{
		description: "flatten un-hierarchical JSON, should remain unchanged",
		input: []byte(`{
	"another_thing": 42,
	"is_winter_coming": true,
	"thing": "val"
}`),
		output: []byte(
			`{
    "another_thing": 42,
    "is_winter_coming": true,
    "thing": "val"
}`),
	},

	{
		description: "flatten a heirarchical JSON",
		input: []byte(`{
	"a": 1,
	"b": true,
	"c": {
		"d": 42.5,
		"e": {
			"f": "meep",
			"g": 5
		},
		"h": "thingy.thing"
	}
}`),
		output: []byte(`{
    "a": 1,
    "b": true,
    "c.d": 42.5,
    "c.e.f": "meep",
    "c.e.g": 5,
    "c.e.h": "thingy.thing"
}`),
	},
}

func TestJSONConverstion(t *testing.T) {
	for _, entry := range jsonTable {
		fmt.Printf("RUNNING: '%s'\n", entry.description)
		var loadedJSON interface{}
		err := json.Unmarshal(entry.input, &loadedJSON)
		if err != nil {
			fmt.Printf("error unmarshaling json: %s", err)
			t.Fail()
		}

		result := map[string]interface{}{}
		flattenJSON("", loadedJSON.(map[string]interface{}), result)

		// Marshal the result map into JSON format.
		res, err := json.MarshalIndent(result, "", "    ")
		if err != nil {
			fmt.Printf("error marshaling JSON: %s", err)
			t.Fail()
		}

		if !bytes.Equal(res, entry.output) {
			fmt.Printf("expected:\n%s\nto be:\n%s\n", res, entry.output)
			t.Fail()
		}
	}
}
