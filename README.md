# JSONStraightener (jstr)!

JSONStraightener is just like a hair straightener but for JSON! `jstr` for short.
It takes a JSON file (hierarchical) and converts it into a flat JSON (path to leaf value)

## Usage
```bash
jstr --help
Usage of ./jstr:
  -debug
    	print debug logs
  -file string
    	path to the JSON file
```

## Example
```bash
jstr -file ./sample.json
INFO[0000] result map                                    result="map[a:1 b:true c.d:42.5 c.e.f:meep c.e.g:5 c.e.h:thingy.thing]"
INFO[0000] final result:
{
    "a": 1,
    "b": true,
    "c.d": 42.5,
    "c.e.f": "meep",
    "c.e.g": 5,
    "c.e.h": "thingy.thing"
}
```

Where `sample.json` is:
```json
{
	"a": 1,
	"b": true,
	"c": {
		"d": 42.5,
		"e": {
			"f": "meep",
			"g": 5
		},
		"h": "thingy.thing"
	}
}
```

## How to build

To build this, I have some make targets.

### Update vendor

`make vendor`

### Build binary

`make build`

### Run tests

`make tests`

## Known issues
- Doesn't handle array JSON objects (as mentioned in the question)
- If not uring Go 1.10+ the tests can be a bit flaky because assertion order matters (could use an assertion library like Gomega to avoid this)