vendor:
	go get -u github.com/Masterminds/glide
	glide up -v

build: vendor
	GOOS=linux GOARCH=amd64 go build -o bin/jstr-linux .
	GOOS=darwin GOARCH=amd64 go build -o bin/jstr-dawrin .

tests:
	go test -v ./...